package com.metlife.capstone.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws UnknownHostException {
    	
    	MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
    	DB db = mongoClient.getDB( "test" );
    	System.out.println("hello");
    	
    	Set<String> colls = db.getCollectionNames();
    	for (String s : colls) {
    	    System.out.println(s);
    	}
    	
    	BasicDBObject doc = new BasicDBObject("name", "Gokul")
        .append("type", "database")
        .append("count", 1)
        .append("info", new BasicDBObject("x", 203).append("y", 102));

    	 
    	DBCollection coll = db.getCollection("pm");
    	coll.insert(doc);
    	DBCursor cursor = coll.find();
    	try {
    		   while(cursor.hasNext()) {
    		       System.out.println(cursor.next());
    		   }
    		} finally {
    		   cursor.close();
    		}
    	
    	
    	
    	System.out.println("end");
    	
    }
}
